/*
Copyright 2019 New Vector Ltd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

import React from 'react';
import classNames from "classnames";
import Web3 from 'web3'

import SdkConfig from '../../../SdkConfig';
import AuthPage from "./AuthPage";
import { _td } from "../../../languageHandler";
import SettingsStore from "../../../settings/SettingsStore";
import { UIFeature } from "../../../settings/UIFeature";
import LanguageSelector from "./LanguageSelector";
import EmbeddedPage from "../../structures/EmbeddedPage";
import { MATRIX_LOGO_HTML } from "../../structures/static-page-vars";
import dis from "../../../dispatcher/dispatcher";
// translatable strings for Welcome pages
_td("Sign in with SSO");

interface IProps {

}

export default class Welcome extends React.PureComponent<IProps> {

    state = {
        address:'none'
    }
    

    async componentDidMount(){
        // await this.loadweb3()
    }

    async loadweb3(){
        console.log('window-------------------')
        let web3js;
        if ((window as any).ethereum) {
            web3js = new Web3((window as any).ethereum)
            // console.log(web3js)
            try {
                let acc = await (window as any).ethereum.enable()
                if(acc){
                    dis.dispatch({
                        action: 'start_login',
                        params: acc[0],
                    });
                }
            } catch (error) {
                console.log(error.message)
            }
            (window as any).web3js = web3js
        }else{
            alert('Please open the MetaMask')
        }
    }

    public render(): React.ReactNode {
        const pagesConfig = SdkConfig.getObject("embedded_pages");
        let pageUrl = null;
        if (pagesConfig) {
            pageUrl = pagesConfig.get("welcome_url");
        }
        if (!pageUrl) {
            pageUrl = 'welcome.html';
        }

        return (
            <AuthPage>
                <div className={classNames("mx_Welcome", {
                    mx_WelcomePage_registrationDisabled: !SettingsStore.getValue(UIFeature.Registration),
                })}>
                    
                    <EmbeddedPage
                        className="mx_WelcomePage"
                        url={pageUrl}
                        replaceMap={{
                            "$riot:ssoUrl": "#/start_sso",
                            "$riot:casUrl": "#/start_cas",
                            "$matrixLogo": MATRIX_LOGO_HTML,
                            "[matrix]": MATRIX_LOGO_HTML,
                        }}
                    />
                    <div className='mx_ButtonRow_div' onClick={this.loadweb3}>
                        {/* <img width="40" height="40" src={require('./metamask.png')} alt="" /> */}
                        <img src="welcome/images/metamask.png" alt="" className="mx_Logo"/>
                        <div className='mx_ButtonLabel'>
                            MetaMask
                        </div>
                    </div>
                    {/* <div className='address'>address:{this.state.address}</div> */}
                    <LanguageSelector />
                </div>
            </AuthPage>
        );
    }
}
